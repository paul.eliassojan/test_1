## Day 2 

## Inroduction to JavaScript

:one:
 ```
 JavaScript can "display" data in different ways:

    Writing into an HTML element, using innerHTML.
    Writing into the HTML output using document.write().
    Writing into an alert box, using window.alert().
    Writing into the browser console, using console.log().
```

:two: JavaScript uses the `var keyword` to declare variables.

:three: Events: 

#### HTML elements can trigger an event when something happens to HTML elements

 ```
 <body>

<button onclick="displayAlert()">click here</button>

<script>
function displayAlert(){
    alert('Hello word')
    // some more code
    }
</script>
</body>

or 

<button onclick="(()=>{
    alert('Hello world');
    console.log('Hello world');
    //more statements
    })()">click here</button>
```

:four: Operators
 `Add, sub, multi etc`

:five: JavaScript Objects

