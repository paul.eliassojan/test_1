import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./home";
import News from "./news";
import Contact from "./contact";
import About from "./about";

function App() {
  return (
    <Router>
      <div class="topnav">
        <a href="/home">Home</a>
        <a href="/news">News</a>
        <a href="/contact">Contact</a>
        <a href="/about">About</a>
      </div>

      <Route exact path="/home" component={Home} />
      <Route exact path="/news" component={News} />
      <Route exact path="/contact" component={Contact} />
      <Route exact path="/about" component={About} />
    </Router>
  );
}

export default App;
